package agenda.test;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestLab2 {

    private Contact con;
    private RepositoryContact rep;

    @Before
    public void setUp() throws Exception {
        rep = new RepositoryContactMock();
    }

    @Test
    public void testCase1()
    {
        try {
            con = new Contact("Raul Paros", "Strada Stefan cel Mare nr. 147, Sibiu", "+40751040594");
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }

        rep.addContact(con);
        for(Contact c : rep.getContacts())
            if (c.equals(con))
            {
                assertTrue(true);
                break;
            }
    }

    @Test
    public void testCase2()
    {
        try {
            con = new Contact("Raul Paros", "", "+40751040594");
            rep.addContact(con);
        } catch (Exception e) {
            assertTrue(true);
        }

    }

    @Test
    public void testCase3()
    {
        try {
            con = new Contact("", "Strada Stefan cel Mare nr. 147, Sibiu", "+40751040594");
            rep.addContact(con);
        } catch (Exception e) {
            assertTrue(true);
        }
    }

    @Test
    public void testCase4()
    {
        try {
            con = new Contact("Raul Paros", "Strada Stefan cel Mare nr. 147, Sibiu", "");
            rep.addContact(con);
        } catch (Exception e) {
            assertTrue(true);
        }

    }

    @Test
    public void testCase5()
    {
        try {
            con = new Contact("Andrei Suciu", "Calea Motilor 62, Cluj-Napoca", "758828371");
            rep.addContact(con);
        } catch (Exception e) {
            assertTrue(true);
        }

    }


    @Test
    public void testCase6()
    {
        try {
            con = new Contact("Raul Paros", "Strada Stefan cel Mare nr. 147, Sibiu", "+40751040594");
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }

        rep.addContact(con);
        for(Contact c : rep.getContacts())
            if (c.equals(con))
            {
                assertTrue(true);
                break;
            }
    }

    @Test
    public void testCase7()
    {
        try {
            con = new Contact("Raul Paros", "", "+40751040594");
            rep.addContact(con);
        } catch (Exception e) {
            assertTrue(true);
        }

    }

    @Test
    public void testCase8()
    {
        try {
            con = new Contact("", "Strada Stefan cel Mare nr. 147, Sibiu", "+40751040594");
            rep.addContact(con);
        } catch (Exception e) {
            assertTrue(true);
        }
    }

    @Test
    public void testCase9()
    {
        try {
            con = new Contact("Raul Paros", "Strada Stefan cel Mare nr. 147, Sibiu", "");
            rep.addContact(con);
        } catch (Exception e) {
            assertTrue(true);
        }

    }

    @Test
    public void testCase10()
    {
        try {
            con = new Contact("Andrei Suciu", "Calea Motilor 62, Cluj-Napoca", "758828371");
            rep.addContact(con);
        } catch (Exception e) {
            assertTrue(true);
        }

    }
}
